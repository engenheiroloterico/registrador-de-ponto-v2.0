from tkinter import Button, Entry, Label, PhotoImage, Tk, Canvas
from  tkinter import ttk 
import datetime as dt
import sqlite3

con = sqlite3.connect('registro_pronto.db')
cur = con.cursor()

# CRIA UMA NOVA TABELA
def creat_table():
    cur.execute('''CREATE TABLE IF NOT EXISTS ponto
        (Colaborador, Entrada, Saida)''')

# RESETA BANCO DE DADOS
def reset_bd():
    cur.execute('DROP TABLE ponto')
    for row in table.get_children():
        table.delete(row)
    creat_table()
    
# RETORNA DATA E HORA RECENTE
def current_time():
    now = dt.datetime.now()
    date_actual = dt.datetime(day=now.day, month=now.month,year=now.year, hour=now.hour, minute=now.minute, second=now.second)
    return date_actual

#REGISTRA ENTRADA
def enter():
    get_name = check_name.get().title().strip()
    cur.execute(f'INSERT INTO ponto VALUES ("{get_name}","{current_time()}","")')
    show_row_cad()
    con.commit()
    check_name.delete(0, len(check_name.get()))

#REGISTRA SAIDA E LIMPA O VISOR
def out():
    cur.execute(f'UPDATE ponto SET Saida="{current_time()}" WHERE Colaborador="{check_name.get().title().strip()}"')
    con.commit()

    for row in table.get_children():
        table.delete(row)
    show_table()

def update_record():
    selected = table.focus()
    row = cur.execute(f'SELECT * FROM ponto WHERE Colaborador="{check_name.get().title().strip()}"') # filtra linha
    content = row.fetchall()
    table.insert(parent='',index='end',iid=None,text='',values=(content[0]))
    table.item(selected, text="",values=(content[0])) # atualiza tabela

# -------------------------- TABELA -------------------------------------------
window = Tk()
style = ttk.Style(window)
canvas = Canvas(width = 612, height = 500)
bg = PhotoImage(file='images\Design sem nome.png')
canvas.create_image(306, 250, image = bg)
canvas.pack()

style.configure("Treeview",
                    background="Turquoise",
                    foreground="black",
                    rowheight=20,
                    font=('bold'),
                    )

game_frame = ttk.Frame(window, border=2, borderwidth=5,)
game_frame.place(width=600, height=250, y=7, x=7)

game_scroll = ttk.Scrollbar(game_frame)
game_scroll.place()

table = ttk.Treeview(game_frame,yscrollcommand=game_scroll.set, xscrollcommand =game_scroll.set, padding=2, selectmode='browse')
table.place(width=587, height=240, y=0, x=0)

game_scroll.config(command=table.yview)

table['columns'] = ('col_name', 'col_in', 'col_out')

#COLUNAS
table.column("#0",width=1, stretch=False)

# LINHAS
table.heading("#0",text="",anchor='center')
table.heading("col_name",text='Colaborador',anchor='center')
table.heading("col_in",text="Entrada",anchor='center')
table.heading("col_out",text="Saída",anchor='center')

# EXIBE O ITEM PESQUISADO
def show_row_cad():
    row = cur.execute(f'SELECT * FROM ponto WHERE Colaborador="{check_name.get().title()}"')
    for content in row.fetchall():
        table.insert(parent='',index='end',iid=None,text='',values=(content))
    check_name.delete(0, len(check_name.get()))

def select_name():
    query = check_name.get().title()
    selections = []
    for child in table.get_children():
        if query in table.item(child)['values']:   # compare strings in  lower cases.
            selections.append(child)
    table.selection_set(selections)

#MOSTRA A TABELA
def show_table():
    row = cur.execute(f'SELECT * FROM ponto')
    for content in row.fetchall():
        table.insert(parent='',index=-1,iid=None,text='',values=(content))

# ----------------------------------------------------------------------------------------
window.title('Registrador de Ponto')
window.minsize(width=583, height=502)

label = Label(text='Nome e registro:', bg='Turquoise', fg='Black', font=('Arial', 10,'bold'))
label.place(y=270, x=57)

check_name = Entry(width=30, font=(14))
check_name.place(y=270, x=175)

# REGISTRA ENTRADA
img_check_in = PhotoImage(file='images\REGISTRAR ENTRADA.png')
check_in = Button(image=img_check_in, command=enter, bg='Turquoise')
check_in.place(y=300, x=55)

# REGISTRA SAIDA
img_check_out = PhotoImage(file='images\REGISTRAR SAIDA.png')
check_out = Button(image=img_check_out, command=out, bg='Turquoise')
check_out.place(y=300, x=310)

#PESQUISA
search_name = Button(text='Procurar', width=13, command=select_name, bg='Turquoise', fg='Black', font=('Arial',8,'bold'))
search_name.place(y=270, x=455)

#RESETAR REGISTROS
reset_reg = Button(text='Resetar registros', width=14, command=reset_bd, bg='Turquoise', fg='Black', font=('Arial',8,'bold'))
reset_reg.place(y=372, x=55)

creat_table()
show_table()
window.mainloop()