<div align=center><h1> Registro de ponto em Python Versão 2.0</h1> </div>
<div align=center><img width=370 src="https://gitlab.com/engenheiroloterico/registrador-de-ponto-v2.0/-/raw/main/public/registro%20de%20ponto%20em%20python.PNG"> </div>
<div align=center><p align='center'> Sistema simples de um registro de ponto usando a biblioteca 
TKINTER da linguagem de programação Python </p></div>

<div><h1> Estilo</h1> </div>

<p align='center'> Para deixar a aparência da interface foi ultilizada a ferramenta Canvas recurso online para montar planos de fundo personalzados</p>
<hr>

<h1 align='center'> Tecnologias Utilizada</h1>
<ul>
<li>Python</li>
<ul>
<li>linguagem de programação de alto nível, interpretada de script, imperativa, orientada a objetos, funcional, de tipagem dinâmica e forte.</li>
</ul>
<li>Tkinter</li>
<ul>
<li>Baseada em Tcl/Tk, a Tkinter acompanha a distribuição oficial do interpretador Python. É a biblioteca padrão da linguagem Python. Normalmente usada para criar interfaces gráficas.</li>
</ul>
</ul>
<hr>
<h1 align='center'> Ambiente de desenvolvimento</h1>
<p align='center'> O projeto foi desenvolvito na IDE VSCODE, sistema operacional Windows 10, para rodar o projeto na sua máquina: </p>
<ul>
<li>Leia o arquivo requirements.txt que se encontra no repositório do projeto</li>
<li>siga as instruções do gitlab para clonar o projeto : <a>https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#clone-a-repository</a></li>
<ul>
